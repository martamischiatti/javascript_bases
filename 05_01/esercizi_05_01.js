// ESERCIZIO 1

// - Creare 5 variabili contenenti un numero, scrivere un programma in modo da ottenere la somma tra questi numeri e la media.
// - In console deve uscire la seguente frase: ‘La somma tra i numeri equivale a … 1 …’

console.log(`ESERCIZIO 1`);

let num1 = 13;
let num2 = 67;
let num3 = 1;
let num4 = 98;
let num5 = 145;

console.log(num1);
console.log(num2);
console.log(num3);
console.log(num4);
console.log(num5);

let somma = num1 + num2 + num3 + num4 + num5;
console.log('La somma è ' + somma);

let media = somma / 5;
console.log('La media è ' + media);

//ESERCIZIO 2

// - Scrivi due variabili con l’anno corrente e una data di nascita, e stampa in console:
// - l’età della persona, 
// - quanti anni sono necessari per raggiungere i 100
// - e stampi in console la frase: “Hai 26 anni e ti mancano 74 anni per compierne 100”

console.log(`ESERCIZIO 2`);

let year = 2023;
let birth = 1993;

let age = year - birth;

console.log(`Siamo nel ${year} e hai ${age} anni. Ti mancano ${100 - age} anni per compierne 100`); 



// ESERCIZIO 3

// - Scrivere un programma che dati sette valori relativi alle temperature della settimana
// stabilisca la giornata più calda e quella più fredda.



// Esempio:
//   Input: a = 10, b = -2, c = 31, d = 22, e = 15, f = -6, g = 7
//   “La temperatura piu’ calda e’ 31 quella piu’ fredda -6”

console.log(`ESERCIZIO 3`);

let a = -10;
let b = -2;
let c = 31;
let d = 22;
let e = 15;
let f = -6;
let g = 7;

console.log('Le temperature della settimana sono:', a, b, c, d, e, f, g);

let maxTemp = Math.max(a, b, c, d, e, f, g);
console.log('La temperatura massima è stata ', maxTemp);

let minTemp = Math.min(a, b, c, d, e, f, g);
console.log('La temperatura minima è stata ', minTemp);

console.log(`La temperatura massima è stata ${maxTemp}, la temperatura minima è stata ${minTemp}`);