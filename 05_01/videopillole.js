//VIDEOPILLOLA VARIABILI 

console.log(`VARIABILI`);

let a = 1;

console.log(`A vale : ` + a);

a = 10;

console.log(`A vale : ` + a);

// var ci permette di dichiarare una variabile con uno stesso nome 
//let da errore

// il primo carattere deve essere 
// una lettera
// _
// $
// i nomi sono case sensitive 

console.log(`DATA TYPES`);

//PRIMITIVI - possono conservare valori costituiti da un solo valore

// - Number
// - String - contenuti testuali assegnati a una variabile
// - Boolean 
// - NaN
// - undefined
// - Symbol

// qualunque variabile di tipo non primitivo è di tipo object

let c = 15;
console.log(c);
console.log(typeof c);

let d = `marta`;
console.log(d);
console.log(typeof d);

//Operatori

console.log(`OPERATORI ARITMETICI`);

//modulo usato spesso per sapere se un numero è multiplo di un altro


