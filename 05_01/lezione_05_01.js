// Commento singola linea
/*
  Commento 
  su 
  più righe
*/

console.log(`Introduzione a Jacascript`); //utile in fase di sviluppo

// VARIABILE

// var -- in disuso
// let
// const

//Dichiarare una variabile 
let age;

//Assegnare un valore\Inizializzare la variabile
age = 30;

console.log(age);

// Dichiarare e inizializzare la variabile
let firstname = `Marta`;

console.log(firstname);

console.log(`A maggio compirò ${age} anni`);

//Sovrascrivere la variabile

age = 29;

console.log(`Ora ho ${age}`);

/* 
  Convenzioni dichiarazione variabile

Consentito
let test;
let $test;
let _test;
let test1;
let testTest; -- camel case
let test_test

Non consentino
let 1test;
let test test; -- spazi
let #test;
let test%;
let test 1;

*/

//COSTANTI

const IDENTIFICATORE = 'name'; // per convenzione l'identidicatore si scrive in maiuscolo

console.log(IDENTIFICATORE);

//DATA TYPES

/*
 1- Primitivi
 2- Non primitivi/strutturali
*/

/*
  Primitivi
  - number
  - string
  - boolean
  - undefined
  - null
*/

//NUMBER
console.log(`OPERATORI ARITMETICI`);
let num = 10;
let num2 = 5; 

console.log(num);
console.log(num2);

// Operatori Aritmentici

console.log(`Somma: ${num + num2}`);
console.log(`Differenza: ${num - num2}`);
console.log(`Moltiplicazione: ${num - num2}`);
console.log(`Divisione: ${num / num2}`);
console.log(`Potenza: ${num ** num2}`);
console.log(`Modulo: ${num % num2}`);

let num1 = 1;

console.log(`Incremento/Decremento finito`);

// num1 = num1 + 1;
num1 += 2;
console.log(num1);

num1 +- 2;
console.log(num1);

console.log(`Incremento/Decremento unitario`);
num1++; //incrementare
console.log('num1 ++     ' + num1);

num1--; //Decrementare
console.log(num1);

//STRING
console.log(`STRING`);
let text = `Ciao sono Marta`;
console.log(text);

//Concatenazione
let text1 = `Ciao sono `;
let nome = `Marta`;

console.log(text1 + nome);

console.log(num + ` ` + nome); // Javascript sta cambiando il tipo di dato per restituire un risultato - vedi Type Coercion 

let somma = 10;
console.log(somma);
console.log(typeof somma); // typeof -- Operatore Unario - un elemento solo alla volta

let testo = String(somma); 
console.log(typeof somma);


let testo3 = new String(somma); // Metodo per creare un oggetto stringa (vale anche per Number e Boolean)
console.log(testo3);
console.log(typeof testo3);

console.log(testo);

let numero = `10`;
console.log(numero);
console.log(typeof numero);

let testo2 = Number(numero); 
console.log(testo2);
console.log(typeof testo2);

//BOOLEAN: true\false 

//I booleani sono anche valori risultanti dalle operazioni di confronto
//Servono per verificare se una variabile ha un valore o meno

console.log(Boolean(5));// true -- i numeri e le stringhe restituiscono true

console.log(Boolean(`string`));

//OPERATORI DI CONFRONTO

/*
  - ==
  - ===
  - <
  - >
  - <=
  - >=
  - !=
  - <==
  - >==
  - !==

*/

console.log(`OPERATORI DI CONFRONTO`);

console.log(`1 == "1"     `);
console.log(1 == `1`);

console.log(`1 === "1"     `);
console.log(1 === `1`);

console.log(`1 == 1    `);
console.log(1 == 1);


console.log(`1 === 1     `);
console.log(1 === 1);

console.log(`1 > 3     `);
console.log(1 > 3);

console.log(`1 < 4     `);
console.log(1 < 4);

console.log(`1 != 3     `);
console.log(1 != 3);

console.log(`3 >= 3     `);
console.log(3 >= 3);

console.log(`1 <= 4     `);
console.log(1 <= 4);

console.log(`3 !== "3"     `);
console.log(3 !== `3`);

console.log(`3 !== "3"     `);
console.log(3 !== 3);

//Undefined: quando una variabile è dichiarata ma non inizializzata

console.log(`UNDEFINED`);

let variabile_vuota;
console.log(variabile_vuota);

//Null: la variabile non esiste

//Operatori logici 

/* 
Sono usati principalmente per fare più di un confronto

&& and -- restituisce il primo dato falsy, se non c'è restituisce l'ultimo truthy

|| or -- restituisce il primo dato truthy che trova, se non ce ne sono restituisce l'ultimo falsy

! contrario -- restituisce il contrario

*/

//Ogni tipo di dato può avere valore TRUTHY O FALSY
/*
  Dati Truthy
  - numeri != 0 e != Nan
  - string non vuote
  - true
  - non primitivi

  Dati Falsy
  - 0
  - Nan
  - string vuote
  - false
  - undefined
  - null
*/

console.log(`OPERATORI LOGICI`);

console.log(`10 && true`);
console.log(10 && true);

console.log(`true && 10`);
console.log(true && 10);

console.log(`10 || true`);
console.log(10 || true);

console.log(`true || 10`);
console.log(true || 10);

console.log(`Nan || 10`);
console.log(NaN || 10); //Nan not a number è un valore assorbente, come lo zero nella moltiplicazione

console.log(`!true`);
console.log(!true);

let voto = 6;
console.log(voto);

console.log(`voto <= 5 && voto >= 6`);
console.log(voto <= 5 && voto >= 6);

console.log(`voto <= 5 || voto >= 6`);
console.log(voto <= 5 || voto >= 6);

//MATH

console.log(`MATH`);

let num3 = Math.PI;
console.log(num3);

let temp1 = 16;
let temp2 = 8;
let temp3 = 13;
let temp4 = -1;

console.log(`temp1 = ${temp1}`);
console.log(`temp2 = ${temp2}`);
console.log(`temp3 = ${temp3}`);
console.log(`temp4 = ${temp4}`);

console.log(`La temperatura massima di oggi è ${Math.max(temp1, temp2, temp3, temp4)}`);

console.log(`La temperatura minima di oggi è ${Math.min(temp1, temp2, temp3, temp4)}`);

console.log(`Refactoring`);

let max = Math.max(temp1, temp2, temp3, temp4);
let min = Math.min(temp1, temp2, temp3, temp4);

console.log(`La temperatura massima di oggi è ${max}`);

console.log(`La temperatura minima di oggi è ${min}`);












