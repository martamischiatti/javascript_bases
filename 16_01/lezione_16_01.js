//FUNZIONI ED ESEMPI

console.log(`RECAP FUNZIONI CON ESEMPI`);

// Scrivere una funzione che permetta di preparare e stampare in console il cocktail scelto dall'utente

console.log(`Esempio Cocktail`);

function listCocktail(cocktailName, ingredient1, ingredient2) {
  item1 = ingredient1;
  item2 = ingredient2;
  cocktail = cocktailName;

  return `Per preparare ${cocktail} servono questi ingredienti: ${item1}, ${item2}`;
}

console.log(listCocktail(`Cuba libre`, `Coca-cola`, `Rum`));
console.log(listCocktail(`Gin Tonic`, `Acqua Tonica`, `Gin`));
console.log(listCocktail(`Vodka Lemon`, `Lemonsoda`, `Vodka`));

console.log(``);
console.log(`Esempio Cocktail con array`);

function barman(cocktail, ...mixIngredients) {
  return `Per preparare ${cocktail} servono questi ingredienti: ${mixIngredients}`;
}

console.log(barman('Moscow mule', [`Vodka`, `Ginger Beer`, `Lime`]));

console.log(`Esempio coctail arrow function`);
let barmanArrow = (cocktail, ...mixIngredients) => {
  let text = `Ora prepariamo i coctail`;
  console.log(text);

  return `Per preparare ${cocktail} servono questi ingredienti: ${mixIngredients}`;
}

console.log(barmanArrow(`Moscow mule`, [`Vodka`, `Ginger Beer`, `Lime`]));


//Oggetti ed esempi

console.log(``);  
console.log(`OGGETTI ED ESEMPI`);

//OGGETTO: tipo di dato non strutturale. E' una rappresentazione astratta i un oggetto reale. Rappresentato come una lista non ordinata di elementi, i quali sono rappresentati con una coppia chiave valore 
//CHIAVE: può essere uno stringa o un symbol
//VALORE: può essere qualsiasi cosa 
//Un oggetto può avere delle proprietà e/o dei metodi
//Proprietà -- sono tutte le chiavi il cui valore associato non è una funzione
//Metodo -- tutte le chiavi il cui valore è una funzione

let person = {
  name : `Marta`,
  age : 29, 
  hair : false,
  sayHello: function () {
    return `Ciao a tutti mi chiamo ${this.name}` //this si può indicare all'interno dell'oggetto
  }
}

console.log(person);

console.log(`Accedere ad una proprietà`);

console.log(person.age);
console.log(`Il mio nome è ${person.name}, e ho ${person.age} anni`);
console.log(person.sayHello());

//Modificare il valore di una chiave

console.log(`Modificare una proprietà`);

person.age = 18;

console.log(`La mia età modificata è ${person.age}`);

// Aggiungere una proprietà

console.log(`Aggiungere una proprietà`);

person.height = 166;

console.log(person);

//Rappresentare la rubrica di un telefono.
//CRUD - Create - Read - Update - Delete
//1- Mostrare i contatti all'interno della rubrica 
//2- Mostrare un contatto da ricerca
//3- Aggiungere un contatto
//4- Rimuovere un contatto

console.log(``);
console.log(`Esempio rubrica`);

let rubrica = {
  contacts : [
    {name: `Marta`, number : `3497365484`},
    {name: `Marco`, number : `3497364748`},
    {name: `Andrea`, number : `3467383697`},
    {name: `Giulia`, number : `3467635697`},
    {name: `Marta`, number : `3497324567`}
  ],

  showContacts : function() {
    this.contacts.forEach(contact => console.log(contact))
  },

  showSingleContact : function(searchedName) {
    let filtered = this.contacts.filter(contact => contact.name == searchedName)
    console.log(filtered);
    if (filtered.length == 1) {
      console.log(`Il contatto trovato è ${filtered[0].name}`);
    } else if (filtered.length > 1) {
      console.log(`Sono stati trovati ${filtered.length} contatti`);
      filtered.forEach(contact => (console.log(`Nome: ${contact.name}\n Numero: ${contact.number}`)));
    } else {
      console.log(`Il contatto "${searchedName}" non è presente in rubrica`);
    }
  },

  addContact : function (newContactName, newContactNumber) {
    this.contacts.push({name: newContactName, number : newContactNumber})
  },

  deleteContact : function(deletedName) {
    let filtered = this.contacts.filter(contact => contact.name !== deletedName)
    console.log(filtered);
    this.contacts = filtered;
  }
}

console.log(rubrica.contacts);
rubrica.showContacts();
console.log(``);
rubrica.showSingleContact(`Marta`);
rubrica.showSingleContact(`Giorgio`);
rubrica.addContact(`Carlo`, `3339142735`);
rubrica.showContacts();

console.log(``);
rubrica.deleteContact(`Carlo`);


