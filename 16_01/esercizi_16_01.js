// ESERCIZIO 1

// creare un oggetto persona con diverse proprietà e un metodo che permetta alla persona di salutare

console.log(`ESERCIZIO 1`);

let person = {
  name: `Marta`,
  surname: `Mischiatti`,
  age: 30,

  hello: function() {
    console.log(`Ciao io sono ${this.name} e ho ${this.age} anni!`);
  }
}

person.hello();

// ESERCIZIO 2

// -  creare un oggetto agenda con una proprietà che comprenda una lista di contatti con un nome e un numero di telefono, ed abbia diverse funzionalità tra cui:
// - mostrare tutti i contatti dell’agenda
// - mostrare un singolo contatto
// - eliminare un contatto dall’agenda
// - aggiungere un nuovo contatto
// - modificare un contatto esistente  

console.log(``);
console.log(`ESERCIZIO 2`);

let rubrica = {
  contacts: [
      {'firstname': 'Nicola', 'number': '3331111111'},
      {'firstname': 'Lorenzo', 'number': '3332222222'},
      {'firstname': 'Paola', 'number': '3333333333'},
      {'firstname': 'Jenny', 'number': '3334444444'}
  ],

  showContacts: function() {
    this.contacts.forEach(contact => console.log(contact));
  },

  deleteContact: function(deletedName) {
    let filtered = this.contacts.filter(contact => contact.firstname !== deletedName)
    console.log(filtered);
    this.contacts = filtered;
  }
}

console.log(rubrica);
console.log(`Mostrare tutti i contatti`);
rubrica.showContacts();
console.log(`Eliminare un contatto`);
rubrica.deleteContact(`Paola`);
rubrica.showContacts();


// ESERCIZIO 3

// - creare un oggetto bowling con una proprietà che comprenda una lista di giocatori con un nome e i relativi punteggi, ed abbia diverse funzionalità tra cui:
// - creare 10 punteggi casuali per ogni giocatore:
//     - Suggerimento: questo metodo dovra’ ciclare tutti i giocatori, presenti nell’oggetto bowling, e aggiungere ad ogni proprieta’ scores, dieci punteggi casuali ad ogni giocatore
//     - Utilizzare l’istruzione: Math.floor(Math.random() * (10 - 1 +1) + 1)
// - trovare il punteggio finale per ogni giocatore:
//     - Suggerimento: ordinare l’array in ordine Decrescente (Attenzione! E’ un array di oggetti: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#examples)
// - aggiungere un nuovo giocatore
// - determinare il vincitore
// - EXTRA: impostare una classifica dei giocatori
// - Dati di partenza:
// let bowling = {
// 'players': [
//     {'name': 'Livio', 'scores': []},
//     {'name': 'Paola', 'scores': []},
//     {'name': 'Filippo', 'scores': []},
//     {'name': 'Giuseppe', 'scores': []}
// ],
// ...
// }

console.log(``);
console.log(`ESERCIZIO 3`);

let bowling = {
  players : [
    {'name': 'Livio', 'scores': []},
    {'name': 'Paola', 'scores': []},
    {'name': 'Filippo', 'scores': []},
    {'name': 'Giuseppe', 'scores': []}
  ],

  generateScores : function() {
    this.players.forEach(player => {
    for (let i = 0; i < 10 ; i++) {
        player.scores.push( Math.floor(Math.random() * (10 - 1 +1) + 1))
      }
    })
    return this.generateScores;
  },

  finalScore: function () {
    this.players.forEach(player => {
      player.totalScore = player.scores.reduce((acc, score) => acc + score);
    })
  },

  setWinner: function() {
    this.players.sort((a, b) => b.totalScore - a.totalScore);
    console.log(this.players[0]);
  },

  addPlayer: function(insertName) {
    this.players.push({name: insertName, scores: []})
  }

}

console.log(bowling);
console.log(``);

bowling.generateScores();
console.log(bowling);

console.log(``);
bowling.finalScore();
bowling.setWinner();
console.log(bowling.players);

console.log(``);
bowling.addPlayer(`Maria`);
console.log(bowling.players);

bowling.generateScores();
bowling.finalScore();
bowling.setWinner();

console.log(bowling.players);