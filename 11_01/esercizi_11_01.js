//ESERCIZI0 1

// - Scrivi una funzione che, dato un numero, let numberOfShots, simuli un gioco di dadi tra due utenti.  
//     - Stampare il giocatore dche ha totalizzato più punti, tenendo conto che:
//         - ogni dado ha 6 facce
//         - ogni giocatore tirerà il dado n volte
//         Per ogni giocatore:
//         - generare un numero casuale per ogni tiro che effettuera’,
//         - ed ogni tiro sarà sommato ai precedenti per calcolare il punteggio finale del giocatore rispettivo.
//         Suggerimento:
//         - Usiamo questa formula per generare un numero random
//     Math.floor(Math.random() * (6 - 1) + 1);

console.log(`ESERCIZIO 1`);

let numberOfShots = 10; 



function winner() {
  let player1 = 0;
  let player2 = 0;

  for (let i = 0; i < numberOfShots; i++) {
    player1 = player1 + Math.floor(Math.random() * (6 - 1) + 1);
    player2 = player2 + Math.floor(Math.random() * (6 - 1) + 1);
  }

  console.log(`Giocatore 1: ${player1} punti`);
  console.log(`Giocatore 2: ${player2} punti`);

  if (player1 == player2) {
    return `Hai pareggiato`;
  } else if (player1 < player2) {
    return `Ha vinto Giocatore 2 con ${player2}`;
  } else {
    return `Ha vinto Giocatore 1 con ${player1}`;
  }

}

console.log(winner());


console.log(`ESERCIZIO 1 - RETAKE`);

let numberOfShots1 = 20;

function winner2() {
  let playera = 0;
  let playerb = 0;

  for (let i = 0; i < numberOfShots1; i++) {
    playera = playera + Math.floor(Math.random() * (6 - 1) + 1);
    playerb = playerb + Math.floor(Math.random() * (6 - 1) + 1);
  }

  console.log(`Giocatore 1 ha totalizzato: ${playera}`);
  console.log(`Giocatore 2 ha totalizzato: ${playerb}`);

  if (playera == playerb) {
    return `Hai pareggiato`;
  } else if (playera > playerb) {
    return `Giocatore 1 ha vinto con ${playera} punti`;
  } else {
    return`Giocatore 2 ha vinto con ${playerb} punti`;
  }  
}

console.log(winner2());

// ESERCIZIO 2

// -  Scrivere una funzione che permetta di stampare in console tutti i numeri da 1 a N:
//     - N dovra’ rappresentare il parametro formale della funzione
//     - tutti i numeri multipli di 3 siano sostituiti dalla stringa 'Fizz',
//     - tutti i numeri multipli di 5 siano sostituiti dalla stringa 'Buzz'
//     - e tutti i numeri multipli di 15 siano sostituiti dalla stringa 'fizzBuzz'

console.log(``);
console.log(`ESERCIZIO 2`);

  let N;

  function fizzBuzz(N) {
    for(i = 1; i <= N; i++) {

      if (i % 15 == 0) {
        console.log(`Fizz Buzz`); 
      } else if (i % 3 == 0) {
        console.log(`Fizz`); 
      } else if (i % 5 == 0) {
        console.log(`Buzz`); 
      } else {
        console.log(i);
      }
    }
    return i;
  }

console.log(fizzBuzz(10));
console.log(``);
console.log(fizzBuzz(20));
console.log(``);
console.log(fizzBuzz(30));


console.log(`ESERCIZIO 2 - RETAKE`);

let N1;

function fizzBuzz1(N1) {
  for (ii = 1; ii <= N1; ii++) {
    if (ii % 15 == 0) {
      console.log(`Fizz Buzz`);
    } else if (ii % 3 == 0) {
      console.log(`Fizz`);
    } else if (ii % 5 == 0) {
      console.log(`Buzz`);
    } else {
      console.log(ii);
    }
  }  
  return ii;
}

console.log(fizzBuzz1(10));
console.log(``);
console.log(fizzBuzz1(15));
console.log(``);
console.log(fizzBuzz1(60));


// ESERCIZIO 3

// - Scrivi due funzioni:
//     - dato un array: let numbers = [10, 12, 78, -4, -20, 11];
//     - una che prenda in input un array di numeri
//     - e restituisca il maggiore
//     - l'altra che restituisca il minore.
//     - Suggerimento utilizzare Math.max() e Math.min(); consultare per bene la documentazione(ATTENZIONE!):cohttps://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/max?retiredLocale=it#examples

console.log(``);
console.log(`ESERCIZIO 3`);

let numbers = [10, 12, 78, -4, -20, 11];
console.log(numbers);

function setMax() {
  let maxNumbers = numbers.reduce((a, b) => Math.max(a, b));
  return maxNumbers;
}

function setMin() {
  let minNumbers = numbers.reduce((a, b) => Math.min(a, b));
  return minNumbers;
}

console.log(`Il numero massimo è ${setMax()}, il numero minimo è ${setMin()}`);

console.log(`ESERCIZIO 3 - RETAKE`);

let numbersRetake = [11, 13, 79, -5, -21, 12];
console.log(numbersRetake);

console.log(`Il numero massimo è ${Math.max(...numbersRetake)}, il numero minimo è: ${Math.min(...numbersRetake)}`);

// ESERCIZIO 4

// -  Scrivi un programma che dato un array di 10 numeri interi ordinati in modo casuale li riordini in modo decrescente.
//     - Esempio:
//             Input: array = [3, 7, -2, 5, 8, 1, 2, 5, 6, -4]
//             Output: [8, 7, 6, 5, 3, 2, 1, -2, -4]
//     - Variante:
//      Prova ad ordinali in modo crescente.


console.log(``);
console.log(`ESERCIZIO 4`);

let numbers1 = [3, 7, -2, 5, 8, 1, 2, 5, 6, -4];
console.log(numbers1);

numbers1.sort((a, b) => a - b);

console.log(numbers1);

numbers1.sort((a, b) => b - a);

console.log(numbers1);

console.log(`ESERCIZIO 4 - RETAKE`);

let arrayRetake = [3, 7, -2, 5, 8, 1, 2, 5, 6, -4];
console.log(arrayRetake);

console.log(arrayRetake.sort((a, b) => a - b));
console.log(arrayRetake.sort((a, b) => b -a));


// ESERCIZIO 5

// - Scrivi una funzione che dato un numero intero (massimo 9999) conti da quante cifre è formato.
//     - Esempi:
//         Input : 9
//         Output :  1 cifra
    
//         Input : 99
//         Output :  2 cifre
//     - Suggerimento: cercare su internet come contare i caratteri di un dato di tipo number

console.log(``);
console.log(`ESERCIZIO 5`);

function count (num) {
  let digits = num.toString().length;
  return digits;
}

console.log(count(Math.floor(Math.random()* (10000 - 1) + 1)));
console.log(count(2));
console.log(count(3356));
console.log(count(35));

console.log(`ESERCIZIO 5 - RETAKE`);

function countDigits (num) {
let digitsRetake = num.toString().length;
return digitsRetake;
}

console.log(countDigits(3456));
console.log(countDigits(1543727628495771));
console.log(count(Math.floor(Math.random()* (10000 - 1) + 1)));



