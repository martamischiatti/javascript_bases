//FUNZIONI
console.log(`FUNZIONI`);

function saluta(nome, cognome = `Default`) {
  console.log(`Ciao ${nome} ${cognome}`);
}

saluta(`Marta`, `Bianchi`);
saluta(`Gianluca`, `Rossi`);
saluta(`Simone`);

function addFive(number) {
  return number + 5;
}

let risultato = addFive(10);

console.log(risultato);

//Arrow Function

let addTen = (numero) => {
  return numero + 10;
}

let addTen1 = (numero) => numero + 10;

//OGGETTI -- possono contenere più tipi di dati, anche diversi. collezzione di proprietà formate da una coppia chiave valore.

console.log(``);
console.log(`Oggetti`);

let object = {
  nome: `aulab`,              //proprietà dell'oggetto
  indirizzo: `indirizzo`,

  getCoffe: function (){          //metodi dell'oggetto
    console.log(`Coffe Function`);
  },

  presentati: function(){
    console.log(`Ciao benvenuto ${this.nome}`);
  }
}

console.log(object);
console.log(object.nome);  //Top syntax
console.log(object.indirizzo);

console.log(object[`nome`]); //bracket notation

object.getCoffe();
object.presentati();

//Assegnare - sovrascrivere una proprietà

object.nome = `nome riassegnato`;
console.log(object);

object.newProperty = `Nuova proprietà`;
console.log(object);

object.newFunction = function() {
  console.log(`Nuova funzione`);
}

console.log(object);

object.newFunction();
object.presentati();

//Metodi più utilizzati

//keys: restituisce tutte le chiavi dell'oggetto

console.log(Object.keys(object));

//values: restituisce i valori

console.log(Object.values(object));

//entries: restituisce un array di array, ognuno costituito da ciascuna coppia chiave - valore

console.log(Object.entries(object));

//ARRAY - sono oggetti, tipi di dati strutturali

console.log(``);
console.log(`ARRAY`);

let linguaggi = [`HTML`, `CSS`, `Javascript`, `Phyton`, `PHP`];
console.log(linguaggi); 

console.log(linguaggi[2]);

linguaggi[2] = `sovrascritto`;
console.log(linguaggi);

linguaggi[2] = `sovrascritto`;
console.log(linguaggi);

linguaggi.unshift(`aggiungo all'inizio`);
console.log(linguaggi);

linguaggi.push(`aggiungo alla fine`);
console.log(linguaggi);

linguaggi.shift(); //rimuovo dall'inizio
console.log(linguaggi);

let first = linguaggi.shift(); // e restituisco l'elemento eliminato
console.log(first);

linguaggi.pop(); //rimuovo dalla fine
console.log(linguaggi);

let end = linguaggi.pop(); // e restituisco l'elemento eliminato
console.log(end);

linguaggi = linguaggi.concat([`JAVA`, 'C++', `Node.Js`]); // concatenare due array
console.log(linguaggi);

for (let i = 0; i < linguaggi.length; i++) {
  const element = linguaggi[i];

  console.log(element);
  
}

console.log(``);
console.log(`For Each`);

linguaggi.forEach(el => {
  console.log(el);
});

let maiuscola = linguaggi.map((el) => {
  return el.toUpperCase();
})

console.log(maiuscola);

let filtered = linguaggi.filter((el) => {
  return el.length > 4;
})

console.log(filtered);
console.log(linguaggi);


let numbers = [1, 46, 34, 25, 78];
let reduced = numbers.reduce((tot, el) => {
  tot = tot + el;
  return tot;
});

console.log(reduced);
console.log(numbers);