//FUNZIONI - blocco di istruzioni che accetta uno o più paramentri in input, ma restituisce un solo output

//Parametri formali e reali
//Formali - si inseriscono all'interno delle tonde, sono dei segnaposto
//Reali - si usa quando si invoca la funziona, e si passa il valore effettivo che deve assumere

console.log(`FUNZIONI`);

let name1 = `Francesco`;
let name2 = `Andrea`;
let name3 = `Sebastiano`;
let name4 = `Giulia`;

// console.log(`Ciao sono ${name1}`);
// console.log(`Ciao sono ${name2}`);
// console.log(`Ciao sono ${name3}`);
// console.log(`Ciao sono ${name4}`);

function welcome(nome) { //Dichiarazione

  //Corpo della funzione: codice eseguito quando viene chiamata la funzione

  return `Ciao sono ${nome}`; //restituisce l'output e blocca il codice 

}

console.log(welcome(name1));
console.log(welcome(name2));
console.log(welcome(name3));
console.log(welcome(name4)); 

/*
  Scrivere un programma che simuli un gioco di dadi tra due giocatori:
  - impostare il numero di tiri
  - calcolare il punteggio finale per ogni giovatore
  - determinare il vincitore 
*/
console.log(``);
console.log(`Simulatore gioco dadi`);



let shoot = 5;

function setWinner(numeroTiri, ) {
  let player1 = 0; //le inizializzo all'interno della funzione in modo che ogni volta che viene chiamata la funzione il punteggio si riazzera
  let player2 = 0;

  for (let i = 0; i < numeroTiri; i++) {
    player1 = player1 + Math.floor(Math.random() * (6 - 1) +1); 
    player2 += Math.floor(Math.random() * (6 - 1) +1);
  }
  
  // console.log(`Giocatore 1: ${player1} punti`);
  // console.log(`Giocatore 2: ${player2} punti`);
  
  // while (player1 == player2) {
  //   for (let i = 0; i < shoot; i++) {
  //     player1 = player1 + Math.floor(Math.random() * (6 - 1) +1); 
  //     player2 += Math.floor(Math.random() * (6 - 1) +1);
  //   }
  // }
  
  if (player1 == player2) {
    console.log(`Pareggio`);
  } else if (player1 < player2) {
    return `Vince Giocatore 2 con ${player1} punti`;
  } else {
    return `Vince giocatore 1 con ${player2} punti`;
  }
}

console.log(setWinner(5)); 
console.log(setWinner(10)); 
console.log(setWinner(20)); 

//Arrow Function
console.log(``);
console.log(`Arrow Function`);



let somma = (num1, num2) => num1 + num2;
console.log(somma(2, 5));

//DATI STRUTTURALI - ARRAY
// tipo di oggetto - lista ordinata di elementi, elencati attraverso un indice che parte da 0

console.log(``);
console.log(`ARRAY`);

let array = [1, 4, `ciao`, [7, `bello`]];
console.log(array);

//Accedere ad un elemento

console.log(array[2]);
console.log(array [3][1]);
console.log(array [1], array[2]);

console.log(array.length);

//Cambiare un elemento
array[1] = `mi hanno camnbiato`;
console.log(array);

console.log(``);
console.log(`For con Array`);



let numbers = [1, 2, 6, 37, 31, 19, 34, 39];

for (let i = 0; i < numbers.length; i++) {
  if (numbers[i] % 2 == 0) {
  console.log(numbers[i]);  
  }
}

//Metodi degli array

console.log(``);
console.log(`Metodi degli array`);

let myarray = [`inizio`];
console.log(myarray);

//push: permette di inserire uno o più elementi alla fine di un array
myarray.push(47, 1, 72);

console.log(myarray);

//pop: permette di rimuovere l'ultimo elemento di un array
myarray.pop();

console.log(myarray);

//shift: permette di rimuovere il primo elemento alla fine di un array
myarray.shift();

console.log(myarray);

//unshift: permette di aggiungere un elemento all'inizio dell'array
myarray.unshift(`nuovo inizio`);

console.log(myarray);

//splice(indice, num elementi, aggiungi altro elemento)
myarray.splice(2, 1, `splice-element`);

console.log(myarray);

//sort: permette di ordinare gli elementi di un array
let numbers2 = [43, 21, -7, -23, 16, 101];
console.log(numbers2);

numbers2.sort((a, b) => a - b); //funzione callback - quando viene eseguita come parametro

console.log(numbers2);

numbers2.sort((a, b) => b - a); //funzione callback - quando viene eseguita come parametro

console.log(numbers2);

let sortString = [`Morgana`, `Andrea`, `Marta`, `Giacomo`, `Lisa`]

console.log(sortString);
console.log(sortString.sort());
console.log(sortString.sort().reverse());