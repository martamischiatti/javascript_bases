//push
console.log(`Metodo push con operazioni`);

let array = [13, 67, -6, 45, 23];
let newArray = [];

console.log(`Array di partenza: ${array}`);
console.log(`Array di partenza: ${newArray}`);

//Inserire i numeri del primo array moltiplicati per due 

for (let i = 0; i < array.length; i++) {
  console.log(array[i]);   
  newArray.push(array[i] * 2);
}

console.log(array);
console.log(newArray);


//Operazioni con le stringhe
//string: tipo di dato particolare - array-like, possono sfruttare alcune proprietà degli array

console.log(``);
console.log('Oprazioni con le stringhe');

let text = `Ciao a tutti`;

console.log(text);

console.log(text.length);

//split: permette di trasformare una stringa in un array; vuole un separatore che indichi in che modo deve trasformare una stringa in un array - quando deve separare gli elementi

console.log('Metodo split');

let arrayText = text.split(``);

console.log(arrayText);

let reverseText = arrayText.reverse();

console.log(reverseText);

//join: permette di trasformare un array di stringhe in una stringa. Vuole lo stesso parametro di split
console.log('Metodo join');

let joinText = reverseText.join(``);

console.log(joinText);

//every: permette di verificare che tutti gli elementi all'interno dell'array soddisfino una determinata condizione. Restituisce true/false. Come parametro vuole una call back

console.log('Metodo every');

let numbers = [1, 2, 3, 4, 5];

let isChecked = numbers.every((element => element < 6));

console.log(isChecked);

isChecked = numbers.every((element => element < 3));

console.log(isChecked);

//some: permette di verificare che almeno un elemento dell'array soddisfi una determinata condizione. Restituisce un booleano.

console.log('Metodo some');

let hasChecked = numbers.some((element => element < 0));

console.log(hasChecked);

hasChecked = numbers.some((element => element < 3));

console.log(hasChecked);

//PRIMITIVI: il passaggio è per valore e quindi sono detti immutabili 
//NON PRIMITIVI: il passaggio è per Riferimento, sonno detti mutabili
console.log(``);
console.log(`PASSAGGIO PER VALORE/RIFERIMENTO`);

let num1 = 2;
let num2 = 4;

num1++;

console.log(num1);
console.log(num2);  //con i dati ptimitivi si creano due locazioni di memoria distinte

let myArray = [1, 2, 3];
let copyArray = myArray;

myArray[0] = `ciao`;

console.log(myArray);
console.log(copyArray); // con i dati strutturali si creano due chiavi per accedere alla stessa locazione -- bisogna creare un clone dell'array originale e lavorare su quello per non perdere i dati.

//concat: permette di unire due array, e di crearne uno nuovo

console.log('Metodo concat');

let numbers1 = [1, 2, 3];
let emptyArray = [];

let newArray1 = emptyArray.concat(numbers1);

console.log(numbers1);
console.log(emptyArray);
console.log(newArray1);

newArray1[0] = `cambiato`;

console.log(numbers1);
console.log(newArray1);

//spread operator (...) - permette di estrare i dati primitivi da un array

console.log(``);
console.log(`Spread Operator`);

console.log(Math.max(...numbers1));

console.log(``);
console.log(`Esempio`);

// Scrivere una funzione che mi permetta di calcolare la somma di un array di numeri

let arrayNumber = [1, 2, 3, 4, 5];
let accumulatore = 0;

for (let i = 0; i < arrayNumber.length; i++) {
  let element = arrayNumber[i];
  accumulatore = accumulatore + element; 
}

console.log(accumulatore);

//reduce: trovare la somma di ridurre l'intero contenuto di un array in un solo dato. Vuole due parametri l'accumulatore e l'elemento
console.log(`Esempio con reduce`);
let somma = arrayNumber.reduce((accumulatore, element) => accumulatore + element);

console.log(somma);

console.log(`Esempio media`);

let media = arrayNumber.reduce((accumulatore, element) => accumulatore + element) / arrayNumber.length;

console.log(media);

//map: crea un clone di un array e permette di modificare gli elementi all'interno di un array. Come paramentro vuole una call back

console.log(``);
console.log(`Metodo map`);

let arr1 = [10, 11, 12];
console.log(arr1);
let newArr = arr1.map(element => element * 2);
console.log(newArr);
let arr2 = arr1.map(element => element);
console.log(arr2);

//Ottenere un nuovo array che abbia come valori i numeri del primo array sommati con quelli del secondo del secondo array.

console.log(`Esempio map`);

let mapNumbers = [1, 2, 3, 4];
let mapNumbers2 = [10, 11, 12, 13, 14];
let newMapArray = mapNumbers.map((numero, index) => numero + mapNumbers2[index]); 

console.log(mapNumbers);
console.log(mapNumbers2);
console.log(newMapArray);

//forEach: permette di ciclare ogni elemento di un array. 

console.log(``);
console.log(`Metodo forEach`);

let mixArray = [`ciao`, 23, `stringa`, 77];
console.log(mixArray);

mixArray.forEach(element => console.log(element));

let items = mixArray.forEach(element => {
  return element
});

console.log(items); //Il metodo for each (che è una funzione) restituisce sempre undefined -- si usa per visualizzare un'informazione a video, non per restituire qualcosa.

//filter: permette di filtrare uno o più elementi all'interno di un array in base a una condizione. Restituisce sempre un nuovo array. 

console.log(``);
console.log(`Metodo Filter`);

let filterNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
console.log(filterNumbers);

let newFilter = filterNumbers.filter(element => element % 2 == 0);

console.log(newFilter);

console.log(``);
console.log(`Usare più metodi`);

let message = `Ciao a tutti`;

console.log(message);

// let messageArray = message.split(``);
// console.log(messageArray);

// let messageArray = message.split(``).filter(element => element != `t`);
// console.log(messageArray);

let messageArray = message.split(``).filter(element => element != `t`).join(``);
console.log(messageArray);

console.log(``);

console.log(mixArray);
let filteredNumbers = mixArray.filter(element => typeof element ==`number`);

console.log(filteredNumbers);

//includes: permette di determinare se un certo valore è incluso in un array. Restituisce sempre un booleano.  Come parametro vuole una call back.

console.log(``);
console.log(`Metodo includes`);
let mixArray1 = [`ciao`, 5, 34, `string`];
console.log(mixArray1.includes(`string`));
console.log(mixArray1.includes(`ciao a tutti`));


//toLowerCase/toUpperCase per trasformare tutto in maiuscolo/minuscolo


console.log(`Esempio includes`);

let test = `ciao a tutti`;
let vocali = [`a`, `e`, `i`, `o`, `u`];
console.log(test);

let final = test.split(``).filter(element => vocali.includes(element));

console.log(final);