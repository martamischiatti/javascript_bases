//ESERCIZIO 1

// - Scrivi una funzione che prenda in input una stringa e restituisca TRUE se è palindroma, FALSE se non lo è.
//     - Nel controllo scarta gli spazi e i segni di punteggiatura.
//             Esempio:
//             Input: “i topi non avevano nipoti”
//             Output: TRUE
//         Consigli:
//     - Suggerimento: Puoi eliminare spazi e segni di punteggiatura usando :  
//      str.replace(/\W/g, "")

console.log(`ESERCIZIO 1`);

let palindroma = `i topi non avevano nipoti`;

palindroma = palindroma.replace(/\W/g, ``);
let palindromaTest = palindroma.replace(/\W/g, ``);

console.log(palindroma);
console.log(palindromaTest);

console.log(palindromaTest = palindromaTest.split(``));

let palindromaReverse = palindromaTest.reverse();

console.log(palindromaReverse);

function palindromaFunction() {
  if (palindroma == palindromaReverse ) {
    retturn `true`;
  } else {
    return`false`;
  }
}

console.log(palindromaFunction());


console.log(`ESERCIZIO 1 - RETAKE`);
// - Scrivi una funzione che prenda in input una stringa e restituisca TRUE se è palindroma, FALSE se non lo è.
//     - Nel controllo scarta gli spazi e i segni di punteggiatura.
//             Esempio:
//             Input: “i topi non avevano nipoti”
//             Output: TRUE
//         Consigli:
//     - Suggerimento: Puoi eliminare spazi e segni di punteggiatura usando :  
//      str.replace(/\W/g, "")

let palindromaRetake = `i topi non avevano nipoti`;
console.log(palindromaRetake);


// ESERCIZIO 2

// 1. Scrivi un programma che dati: - 2 array di 10 elementi interi casuali compresi tra 1 e 10, permetta di effettuare, una delle seguenti operazioni: addizione
// 2.  sottrazione 
// 3. moltiplicazione 
// 4. divisione -
// 5.  Esegua il calcolo tra ogni elemento dei due array, salvando ciascun risultato in un terzo array d’appoggio. - 
// 6. Esempio: Input: a= [3, 7, 2, 5, 8, 1, 2, 5, 6, 4], b= [9, 3, 1, 4, 7, 6, 5, 10, 1, 5], operazione = "addizione" Output: c = [12, 10, 3, 9, 15, 7, 7, 15, 7, 9]


console.log(``);
console.log(`ESERCIZIO 2`);

let a = [3, 7, 2, 5, 8, 1, 2, 5, 6, 4];
let b = [9, 3, 1, 4, 7, 6, 5, 10, 1, 5];

console.log(a);
console.log(b);

let addition = a.map((element, index)  => element + b[index]);
console.log(`Addizione: ${addition}`);

let substraction = a.map((element, index)  => element - b[index]);
console.log(`Sottrazione: ${substraction}`);

let multiplication = a.map((element, index)  => element + b[index]);
console.log(`Moltiplicazione: ${multiplication}`);

let division = a.map((element, index)  => element / b[index]);

console.log(`Divisione: ${division}`);

// ESERCIZIO 3

// - Scrivi un programma che dato un array di numeri, calcoli la media dei valori e restituisca in output la media e tutti i valori minori della media:  
//       Esempio:
//         Input: a = [3, 5, 10, 2, 8]
//     -     Output: media = 5.6, valori minori = [3, 5, 2]
//     -     
//     -   Variante:
//     -   Stampa anche quanti sono i valori minori della media e quanti quelli maggiori. 

console.log(``);
console.log(`ESERCIZIO 3`);

let array = [3, 7, 2, 5, 8, 1, 2, 5, 6, 4, 34, 56, 12, 58, 78, 35, 29, 98, 54, 1, 71];
console.log(array);

let media = array.reduce((acc, element) => acc + element) / array.length;
console.log(`Media: ${media}, i valori minimi sono: ${array.filter(element => element < media)}`);

let minimi = array.filter(element => element < media);
let massimi = array.filter(element => element >= media);


console.log(`I valori minimi sono ${minimi.length}, i valori massimi sono ${massimi.length}`);

// ESERCIZIO 4

// - Scrivi una funzione che permetta di calcolare la somma dei numeri all’interno di un array:
//     - dato di partenza, let numbers = [10, 20, 30, 40];
//     - il risultato dovra’ restituire = 100;

console.log(``);
console.log(`ESERCIZIO 4`);

let numbers = [10, 20, 30, 40];
console.log(numbers);

let somma = numbers.reduce((acc, element) => acc + element);
console.log(`La somma è ${somma}`);

// ESERCIZIO 5

// -  Scrivere un programma che permetta di ottenere un nuovo array che abbia come valori i numeri del primo array sommati con i valori del secondo array:
//     - let numbers1 = [10, 20, 30];
//     - let numbers2 = [40, 50, 60];
//     - dovra’ restituire come risultato, let newArray = [50, 70, 90]

console.log(``);
console.log(`ESERCIZIO 5`);

let numbers1 = [10, 20, 30];
let numbers2 = [40, 50, 60];

console.log(numbers1);
console.log(numbers2);

let newArray = numbers1.map((element, index) => element + numbers2[index]);
console.log(newArray);

// ESERCIZIO 6

// - Scrivere un programma che permetta di filtrare soltanto le parole all’interno di un array:
//     - let mixArray = [1, true, ‘hackademy’, 100, ‘Javascript’, false, null, ‘php’]
//     - il risultato dovra’ dare, let filtered = [‘hackademy’, ‘Javascript’, ‘php’]

console.log(``);
console.log(`ESERCIZIO 6`);

let mixArray = [1, true, `hackademy`, 100, `Javascript`, false, null, `php`];

console.log(mixArray.filter((element) => typeof element == `string`));

// ESERCIZIO 7

// - Scrivere un programma che permetta di stabilire se e’ presente, nell’array di partenza, il marchio ‘’Volvo”:
//     - array di partenza, let cars = ["Saab", "Volvo", "BMW"];
//     - se e’ presente, stampare il console: “Nel garage e’ presente il modello Volvo”
//     - Suggerimento utilizzare il metodo includes()

console.log(``);
console.log(`ESERCIZIO 7`);

let cars = ["Saab", "Volvo", "BMW"];
console.log(cars);  

if (cars.includes(`Volvo`) == true) {
  console.log(`Nel garage è presente il modello ${cars[1]}`);
}
















