// EESERCIZIO 1

// - Scrivere un programma che converta un voto numerico (v) in un giudizio seguendo questi parametri:
// - se v e’ minore di 18,  stampare in console  “insufficiente”
// - se v e’ maggiore uguale a 18 e minore di 21,  stampare in console “sufficiente”
// - se v e’ maggiore uguale a 21 e minore di 24,  stampare in console: “buono”
// - se v e’ maggiore uguale a 24 e minore di 27, stampare in console: “distinto”
// - se v e’ maggiore uguale a 27 e minore uguale 29, stampare in console: “ottimo”
// - se v e’ uguale a 30, stampare in console:  “eccellente”
//     Esempio:
//     let v = 29;
//     Output: Ottimo
// - Cercate di risolvere questo esercizio utilizzando prima if, else e poi con switch, case.

console.log(`Esercizio 1 - If else`);

let v = 25; //prompt('Inserisci il tuo voto')

if (v >= 0 && v < 18 ) {
  console.log(`Il tuo voto è: Insufficiente`);
} else if (v >= 18 && v < 21) {
  console.log(`Il tuo voto è: Sufficiente`);
} else if (v >= 21 && v < 24) {
  console.log(`Il tuo voto è: Buono `);
} else if (v >= 24 && v < 27) {
  console.log(`Il tuo voto è: Distinto`);
} else if (v >= 27 && v <= 29) {
  console.log(`Il tuo voto è: Ottimo`);
} else if (v == 30) {
  console.log(`Il tuo voto è: Eccellente`);
} else {
  console.log(`Inserisci un voto valido`);
}

console.log(`Esercizio 1 - If else - RETAKE 1`);

let v1 = 35; //prompt(`Inserisci il tuo voto`)

if (v1 > 0 && v1 < 18) {
  console.log(`Il tuo voto è: Insufficiente`);
} else if (v1 >= 18 && v1 < 21) {
  console.log(`Il tuo voto è: Sufficiente`);
} else if (v1 >= 21 && v1 < 24) {
  console.log(`Il tuo voto è: Buono`);
} else if (v1 >= 24 && v1 < 27) {
  console.log(`Il tuo voto è: Distinto`);
} else if (v1 >= 27 && v1 <= 29) {
  console.log(`Il tuo voto è: Ottimo`);
} else if (v1 == 30) {
  console.log(`Il tuo voto è: Eccellente`);
} else {
  console.log(`Inserisci un voto valido`);
}


console.log(``);
console.log(`Esercizio 1 - Switch`);

let vSwitch = 27; // prompt('Inserisci il tuo voto');

switch (true) {
  case vSwitch >= 0 && vSwitch < 18:
    console.log(`Il tuo voto è: Insufficiente`);
    break;

  case vSwitch >= 18 && vSwitch < 21:
    console.log(`Il tuo voto è: Sufficiente`);
    break;

  case vSwitch >= 21 && vSwitch < 24:
    console.log(`Il tuo voto è: Buono`);
    break;
  
  case vSwitch >= 24 && vSwitch < 27:
    console.log(`Il tuo voto è: Distinto`);
    break;

  case vSwitch >= 27 && vSwitch <= 29:
    console.log(`Il tuo voto è: Ottimo`);
    break;

  case vSwitch == 30:
    console.log(`Il tuo voto è: Eccellente`);
    break;

  default:
    console.log(`Inserisci un voto valido`);
    break;
}

console.log(`Esercizio 1 - Switch - RETAKE 1`);

let vSwitch1 = -5; // prompt('Inserisci il tuo voto');

switch (true) {
  case vSwitch1 > 0 && vSwitch1 < 18:
    console.log(`Il tuo voto è: Insufficiente`);
    break;

  case vSwitch1 >= 18 && vSwitch1 < 21:
    console.log(`Il tuo voto è: Sufficiente`);
    break;

  case vSwitch1 >= 21 && vSwitch1 < 24:
    console.log(`Il tuo voto è: Buono`);
    break;
  
  case vSwitch1 >= 24 && vSwitch1 < 27:
    console.log(`Il tuo voto è: Distinto`);
    break;

  case vSwitch1 >= 27 && vSwitch1 <= 29:
    console.log(`Il tuo voto è: Ottimo`);
    break;

  case vSwitch1 == 30:
    console.log(`Il tuo voto è: Eccellente`);
    break;
  
  default:
    console.log('Inserisci un voto valido');
    break;
}

// ESERCIZIO 2

// Scrivi un programma che dato un numero, let num = 2, stampi la rispettiva tabellina corrispondente.

console.log(``);
console.log(`ESERCIZIO 2`);

let num = 2;

for (let num = 2; num <= 20; num++) {
  if (num % 2 == 0) {
    console.log(num);
  }  
}

console.log(`ESERCIZIO 2 - RETAKE 1`);

let num1 = 7; //prompt(`Inserisci il numero di cui vuoi stampare la tabellina`);

for (i = num1; i <= (num1 * 10); i++) {
  if (i % num1 == 0) {
    console.log(i);
  }
}



//ESERCIZIO 3

// - Scrivere un programma che stampi i numeri da 1 a 100 andando a capo ogni 10:
// - Output: 
// - 1 2 3 4 5 6 7 8 9 10
// - 11 12 13 14 15 16 17 18 19 20
// - 21 22 23 24 25 26 27 28 29 30
// -  31 32 33 34 35 36 37 38 39 40
//         41 42 43 44 45 46 47 48 49 50
//         51 52 53 54 55 56 57 58 59 60
//         61 62 63 64 65 66 67 68 69 70
//         71 72 73 74 75 76 77 78 79 80
//         81 82 83 84 85 86 87 88 89 90
//         91 92 93 94 95 96 97 98 99 100            
// - Suggerimento: Per andare a capo usa ‘ \n’   ←-indizio importantissimo!
console.log(``);
console.log(`ESERCIZIO 3`);

// let test = ``;

// for (let num1 = 1; num1 < 100; num1++) {

//   if (num1 % 10 == 0) {
//     test = `` + num1 + `\n`;
//   } else {
//     test = num1 + ` `;
//   }  

// }

// console.log(test);

let text = '';

for (let i = 1; i <= 100; i++) {
  text = text + i + ` `;

  if (i % 10 == 0) {
     text = text + `\n`
  }
  
}

console.log(text);

console.log(`ESERCIZIO 3 - RETAKE 1`);

let text1 = ``;

for (let i = 1; i < 100; i++) {
    
  if (i % 10 == 0) {
    text1 = text1 + `\n`
  } else {
    text1 = text1 + i + ` `;
  }
}

console.log(text1);

// ESERCIZIO 4

// - Scrivere un programma che converta una temperatura con una delle seguenti descrizioni:
// - se temperatura e’ minore di 20, stampare “non ci sono piu’ le mezze stagioni”
// - se temperatura e’ maggiore uguale a 30, stampare “lu mare, lu sole, lu ientu”
// - se temperatura e’ minore di 30, stampare “mi dia una peroni sudata”
// - se temperatura e’ minore di 0, stampare “non e’ tanto il freddo quanto l’umidita’”
// - se temperatura e’ minore di -10, stampare “copriti…ancora ti raffreddi”
// - Cercate di risolvere questo esercizio utilizzando prima if else e poi con switch case.

console.log(``);
console.log(`ESERCIZIO 4 - IF`);

let temp = -14; //prompt(`Inserisci la temperatura`);

if (temp < 20 && temp >= 0) {
  console.log(`non ci sono piu’ le mezze stagioni`);
} else if (temp >= 30 && temp < 50) {
  console.log(`lu mare, lu sole, lu ientu`);
} else if (temp < 0 && temp >= -10) {
  console.log(`non e’ tanto il freddo quanto l’umidita’`);
} else if (temp < -10 && temp >= -30) {
  console.log(`copriti…ancora ti raffreddi`);
} else {
 console.log(`Il valore inserito non è valido`); 
}

console.log(`ESERCIZIO 4 - IF - RETAKE 1`);

let temp1 = 88; //prompt(`Inserisci la temperatura`);

if (temp1 >= 0 && temp1 < 20 ) {
  console.log(`non ci sono piu’ le mezze stagioni`);
} else if (temp1 >= 30 && temp1 < 60) {
  console.log('lu mare, lu sole, lu ientu');
} else if (temp1 >= 20 && temp1 < 30 ) {
  console.log(`mi dia una peroni sudata`);
} else if (temp1 >= -10 && temp1 < 0 ) {
  console.log(`non e’ tanto il freddo quanto l’umidita`);
} else if (temp1 >= -40 && temp1 < -10) {
  console.log(`copriti…ancora ti raffreddi`);
} else {
  console.log(`Inserisci una temperatura valida`);
}


console.log(``);
console.log(`ESERCIZIO 4 - SWITCH`);

let tempSwitch = 8; //prompt(`Inserisci la temperatura`);

switch (true) {
  case tempSwitch < 20 && tempSwitch >= 0:
    console.log(`non ci sono piu’ le mezze stagioni`);
    break;

  case tempSwitch >= 30 && tempSwitch < 50:
    console.log(`lu mare, lu sole, lu ientu`);
    break;

  case tempSwitch < 0 && tempSwitch >= -10:
    console.log(`non e’ tanto il freddo quanto l’umidita’`);
    break;

  case tempSwitch < -10 && tempSwitch >= -30:
    console.log(`copriti…ancora ti raffreddi`);
    break;

  default:
    console.log(`Il valore inserito non è valido`); 
    break;
}

console.log(`ESERCIZIO 4 - SWITCH - RETAKE 1`);

let tempSwitch1 = 38; //prompt(`Inserisci la temperatura`);

switch (true) {
  case tempSwitch1 >= 0 && tempSwitch1 < 20:
    console.log(`non ci sono piu’ le mezze stagioni`);
    break;

  case tempSwitch1 >= 30 && tempSwitch1 < 50:
    console.log(`lu mare, lu sole, lu ientu`);
    break;

  case tempSwitch1 >= 20 && tempSwitch1 < 30:
    console.log(`mi dia una peroni sudata`);
    break;

  case tempSwitch1 >= -10 && tempSwitch1 < 0:
    console.log(`non e’ tanto il freddo quanto l’umidita`);
    break;

  case tempSwitch1 >= -40 && tempSwitch1 < -10:
    console.log(`copriti…ancora ti raffreddi`);
    break;
  
  default:
    console.log(`Il valore inserito non è valido`); 
    break;
}

// ESERCIZIO 5

// - Scrivere un programma che simuli un distributore di bevande:
// - fare in modo che l’utente possa inserire il numero corrispondente alla bevanda, mediante l’istruzione prompt:
//     - se inserisce 1, seleziona acqua e quindi stampare in console: “E’ stata selezionata l’acqua”
//     - se inserisce 2, seleziona coca cola e quindi stampare in console: “E’ stata selezionata coca cola”
//     - se inserisce 3, seleziona birra e quindi stampare in console: “E’ stata selezionata birra”
//     - se inserisce qualcosa di diverso, il programma dovra’ riproporre la domanda di partenza

console.log(``);
console.log(`ESERCIZIO 5`);

let bevanda = `1`; //prompt(`Seleziona la tua bevanda:\n (1) acqua\n (2) coca-cola\n (3) birra`);

while (bevanda != `1` && bevanda != `2` && bevanda != `3`) {
  bevanda = `1`; // prompt(`Seleziona la tua bevanda:\n (1) acqua\n (2) coca-cola\n (3) birra`);
}

if (bevanda == `1`) {
  console.log(`Hai selezionato: Acqua`);
} else if (bevanda == `2`) {
  console.log(`Hai selezionato: Coca-cola`);
} if (bevanda == `3`){
  console.log(`Hai selezionato: Birra`);
}

console.log(`ESERCIZIO 5 - RETAKE 1`);

let bevanda1 = `2`; //prompt(`Seleziona la tua bevanda:\n (1) acqua\n (2) coca-cola\n (3) birra`)

while (bevanda1 != `1` && bevanda1 != `2` && bevanda1 != `3`) {
  bevanda1 = `2`; //prompt(`Seleziona la tua bevanda:\n (1) acqua\n (2) coca-cola\n (3) birra`);
}

if (bevanda1 == `1`) {
  console.log(`Hai selezionato: Acqua`);  
} else if (bevanda1 == `2`) {
  console.log(`Hai selezionato: Coca-cola`);
} else if (bevanda1 == `3`) {
  console.log(`Hai selezionato: Birra`);
}

// ESERCIZIO 6

// - crivere un algoritmo che permetta di stabilire se una persona ha i requisiti per poter guidare:
// - se la persona ha piu’ di 17 anni e ha con se’ la patente, allora stampare in console “Puoi guidare”
// - altrimenti, se la persona non rispetta una delle due situazioni, stampare in console: “NON puoi guidare”

console.log(``);
console.log(`ESERCIZIO 6`);

let age = 17; //prompt(`Inserisci la tua età`);
let patente = 1; //prompt(`Hai la patente?:\n (1) Sì\n (2) No`);

if (age >= 18 && patente == `1`) {
  console.log(`Puoi Guidare`);
} else {
  console.log(`Non puoi guidare`);
}

console.log(`ESERCIZIO 6 - RETAKE 1`);

let age1 = 24; //prompt(`Inserisci la tua età`);

while (age1 <= 0 || age1 >= 99) {
  alert(`Inserisci un valore valido`)
  age1 = 24; //prompt(`Inserisci la tua età`);
}

let patente1 = `1`; //prompt(`Hai la patente?:\n (1) Sì\n (2) No`);

if (age1 >= 18 && patente1 == `1`) {
  console.log(`Puoi Guidare`);
} else if (age1 >= 18 && patente1 == `2`) {
  console.log(`Sei maggiorenne ma non puoi guidare senza patente`);
} else if (age1 < 18 && patente1 == `1`, `2`) {
  console.log(`Sei minorenne, devi pazientare ancora ${18 - age1} anni`);
} 