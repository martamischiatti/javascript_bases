console.log(`Oggeto Math`);

let numero = 10;
let numero2 = 23;

console.log(numero);
console.log(numero2);

console.log(`Il valore massimo è: ${Math.max(numero, numero2)}`);

console.log(``);
console.log(`Metodi di Math`);
let float = 12.67;
let float2 = 12.43;

console.log(float);
console.log(float2);

console.log(`Arrotondare per eccesso: ${Math.ceil(float)}`);
console.log(`Arrotondare per eccesso: ${Math.ceil(float2)}`);

console.log(`Arrotondare per difetto: ${Math.floor(float)}`);
console.log(`Arrotondare per difetto: ${Math.floor(float2)}`);

console.log(`Arrotondamento automatico: ${Math.round(float)}`);
console.log(`Arrotondamento automatico: ${Math.round(float2)}`);

console.log(`Generare un numero casuale: ${Math.random()}`); // Senza specifiche lo genera tra 0 e 1, esclude 1 e include 0


let max = 100;
let min = 1;


console.log(`Generare un numero casuale tra due valori: ${Math.random() + (max - min) + min}`); 

console.log(`Generare un numero casuale tra due valori: ${Math.random() + (100 - 1) + 1}`); 

console.log(`Generare un numero casuale tra due valori con arrotondamneto: ${Math.round(Math.random() + (max - min) + min)}`); 

console.log(`Generare un numero casuale tra due valori con arrotondamneto: ${Math.round(Math.random() + (100 - 1) + 1)}`); 


let maxUser = 76; //prompt(`Inserisci il valore massimo`);
let minUser = 3; //prompt(`Inserisci il valore minimo`);

let random = Math.random() + (maxUser - minUser) + minUser;

console.log(Math.round(random));

//CONDIZIONI -- restituisce sempre un booleano. 

// if (condizione) {
//   codice che viene eseguito se la condizione è vera
// }
console.log(``);
console.log(`CONDIZIONI - IF`);

let age = 30;
console.log(age);

if (age >= 18) {
  // Codice che verrà eseguito se la condizione è vera
  console.log(`Sei maggiorenne`);
} else { //Non richiede una condizione
  //Codice che viene eseguito in tutti i casi in cui la condizione non è soddisfatta (restituisce false)
  console.log(`Sei minorenne`);
}

//Operatore Ternario - forma contratta per condizione
// (condizione) ? codice true : codice false

console.log(`Operatore Ternario`);

let age2 = 17;

(age2 >=18) ? console.log(`Sei maggiorenne`) : console.log(`Sei minorenne`);

// Inserire più condizioni
console.log(``);
console.log(`Inserire più condizioni`);

let age3 = 56; // prompt(`Inserisci la tua età`);

if (age3 < 18 && age3 > 0) {
  console.log(`Sei minorenne`);
} else if (age3 >= 18 && age3 < 120) {
  console.log(`Sei maggiorenne`);
} else  { 
  console.log(`Valore non valido`);
}


//Esempio di esercizio 
console.log(``);
console.log(`ESEMPIO DI ESERCIZIO`);

/*
  Scrivere un algoritmo che dato un voto numerico, stampi un giudizio 
*/

let voto = 18;

if (voto < 18 && voto > 0) {
  console.log(`Insufficiente`);
} else if (voto >= 18 && voto < 20) {
  console.log(`Sufficiente`);
}

//SWITCH
// switch(key) -- vuole una chiave che verrà valutata in base ai casi impostati
console.log(``);
console.log(`CONDIZIONI - SWITCH`);

let color = `blue`;

switch (color) {
  case `yellow`:
    console.log(`Il colore scelto è giallo`);
    break; // interrompe il codice se il caso si verifica

  case `red`:
    console.log(`Il colore scelto è rosso`);
    break; 

  default:
    console.log(`Il colore non è disponibile`);
    break;
}

// ESERCIZIO SWITCH
console.log(``);
console.log(`Esercizio voti - switch`);

let voto1 = 19;

switch (true) {
  case voto1 < 18 && voto1 > 0:
    console.log(`Insufficiente`);
    break;

  case voto1 >= 18 && voto1 < 20:
    console.log(`Sufficiente`);
    break; 

  default:
    console.log(`Inserisci un voto valido`);
    break;
}

// CICLI 
//for - quando si ha il numero di cicli
// while - qualcosa che si deve verificare fino al compimento di qualcosa

/*
  for (inizializzazione; termine; iterazione)
  - utilizzato molto con gli array
*/

console.log(``);
console.log(`CICLO - FOR`);

for (let i = 0; i < 100; i++) {
  console.log(i);
}

console.log(``);
console.log(`Esercizio`);
// Stampare i numeri pari 

let num = 0;

for (let num = 0; num < 100; num++) {
  if (num % 2 == 0) {
    console.log(num); 
  }
}

//Stampare i numeri da 1 a 100 sostituendo ai multipli di 3 la parola Fizz, ai multipli di 5 la parola Buzz, e ai multipli di 15 la parola Fizz Buzz
console.log(``);
console.log(`Esercizio 2`);

for (let num1 = 1; num1 < 100; num1++) {

  if (num1 % 5 == 0 && num1 % 3 == 0) {
    console.log(`Fizz Buzz`);
  } else if (num1 % 3 == 0) {
    console.log(`Fizz`);
  } else if (num1 % 5 == 0) {
    console.log(`Buzz`);
  } else {
    console.log(num1);
  }
}

//WHILE 
console.log(``);
console.log(`CICLO - WHILE`);

let num2 = 0;

while (num2 < 35) {
  console.log(num2);
  num2++
}

let bevanda = `1`; //prompt(`Scegli la bevanda:\n 1 acqua\n 2 Coca-cola`);

while (bevanda != `1` && bevanda != `2`) {
  prompt(`Scegli la bevanda:\n 1 acqua\n 2 Coca-cola`);
}

console.log(``);
console.log(`Do while`);

let num3 = 10;

do {
  console.log(`Istruzione da eseguire almeno una volta`);
} while (num3 != 10);